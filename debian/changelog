transifex-client (0.14.2-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Hans-Christoph Steiner ]
  * New upstream version 0.14.2

 -- Hans-Christoph Steiner <hans@eds.org>  Fri, 20 Nov 2020 14:04:12 +0100

transifex-client (0.13.9-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * Bump Standards-Version to 4.4.0.
  * d/control: Remove ancient X-Python3-Version field.
  * Bump Standards-Version to 4.4.1.

  [ Hans-Christoph Steiner ]
  * New upstream version

 -- Hans-Christoph Steiner <hans@eds.org>  Tue, 26 May 2020 19:11:29 +0200

transifex-client (0.13.5-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Convert git repository from git-dpm to gbp layout

  [ Hans-Christoph Steiner ]
  * Standards-Version: 4.3.0: remove get-orig-source
  * add autopkgtest smoke test
  * patch to allow using Debian's python3-six version (Closes: #920606)

 -- Hans-Christoph Steiner <hans@eds.org>  Sun, 27 Jan 2019 20:58:59 +0000

transifex-client (0.13.5-1) unstable; urgency=medium

  * New upstream version 0.13.5

 -- Hans-Christoph Steiner <hans@eds.org>  Sat, 26 Jan 2019 23:56:13 +0100

transifex-client (0.13.1-1) unstable; urgency=medium

  * New upstream release
  * switch to python3 (Closes: #782965)
  * remove Janos Guljas as Uploader (Closes: #849729)

 -- Hans-Christoph Steiner <hans@eds.org>  Mon, 12 Feb 2018 13:36:04 +0100

transifex-client (0.12.4-1) unstable; urgency=medium

  * New upstream release

 -- Hans-Christoph Steiner <hans@eds.org>  Fri, 14 Apr 2017 21:11:05 +0200

transifex-client (0.12.2-1) unstable; urgency=medium

  * New upstream release

 -- Hans-Christoph Steiner <hans@eds.org>  Tue, 13 Sep 2016 10:57:30 +0200

transifex-client (0.11.1+git15~g655c5e9-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Use the six library instead of the one embedded in urllib3.
    - Backport upstream commits.
    - Closes: #817038

 -- Felix Geyer <fgeyer@debian.org>  Sat, 03 Sep 2016 22:17:45 +0200

transifex-client (0.11.1+git15~g655c5e9-1) unstable; urgency=low

  * New upstream release
  * Team upload
  * custom upstream version because upstream messed up a little, it should
    actually v0.11, and upstream had been marking beta releases and 0.11.1

 -- Hans-Christoph Steiner <hans@eds.org>  Wed, 17 Feb 2016 23:33:40 +0100

transifex-client (0.11.1-1) unstable; urgency=low

  * New upstream release
  * add bash completion
  * Raise policy version to 3.9.6.

 -- Hans-Christoph Steiner <hans@eds.org>  Fri, 24 Oct 2014 10:56:54 -0400

transifex-client (0.10-1) unstable; urgency=medium

  * New upstream release. (Closes: #734202)
  * debian/control
    - Raise policy version to 3.9.5.

 -- Janos Guljas <janos@debian.org>  Mon, 06 Jan 2014 15:12:02 +0100

transifex-client (0.9.1-1) unstable; urgency=low

  * New upstream release.
  * Add python-pkg-resources as a dependency. (Closes: #716009)

 -- Janos Guljas <janos@debian.org>  Fri, 12 Jul 2013 15:39:53 +0200

transifex-client (0.9-1) unstable; urgency=low

  * New upstream release.
  * debian/control
    - Remove X-Python-Version .
    - Remove version dependency on python-all.
    - Raise debhelper dependency to 9.
    - Raise policy version to 3.9.4.
    - Change my email address.
  * debian/compat
    - Raise debhelper compatibility to 9.
  * debian/copyright
    - Update copyright years.
    - Change my email address.
  * Remove debian/patches, as no longer needed.
  * debian/rules
    - remove override_dh_auto_test as tests are no longer provided by
      the upstream authors.
  * debian/tx.1
    - Change my email address.

 -- Janos Guljas <janos@debian.org>  Wed, 22 May 2013 20:17:29 +0200

transifex-client (0.8-3) UNRELEASED; urgency=low

  * Use canonical URIs for Vcs-* fields.
  * Remove DM-Upload-Allowed; it's no longer used by the archive
    software.

 -- Jakub Wilk <jwilk@debian.org>  Sun, 05 May 2013 18:40:59 +0200

transifex-client (0.8-2) unstable; urgency=low

  * Exclude tests directory from packaging in global python namespace.
    (Closes: #679489)

 -- Janos Guljas <janos@resenje.org>  Fri, 29 Jun 2012 13:14:09 +0200

transifex-client (0.8-1) unstable; urgency=low

  * New upstream release.
  * Run tests on build:
    - Override dh_auto_test.
    - Add python-mock to Build-Depends.
    - Patch tests to implement TestCase's assertIs method from Python 2.7
      for Python 2.6 support.
  * Remove obsolete or incorrect uses of various python related fiels in
    debian/control
  * Update Format URI in debian/copyright.

 -- Janos Guljas <janos@resenje.org>  Wed, 27 Jun 2012 14:10:08 +0200

transifex-client (0.7.3-1) unstable; urgency=low

  * New upstream release.
  * debian/source/options
    - Ignore transifex_client.egg-info changes after build.
  * Fix a typo in description. (Closes: #663477)
  * Bump standards to 3.9.3.

 -- Janos Guljas <janos@resenje.org>  Wed, 16 May 2012 20:08:15 +0200

transifex-client (0.7.2-1) unstable; urgency=low

  * New upstream release.

 -- Janos Guljas <janos@resenje.org>  Mon, 20 Feb 2012 14:22:04 +0100

transifex-client (0.7-1) unstable; urgency=low

  * New upstream release.
  * Add debian/source/local-options.
  * Update debian/copyright Format value.

 -- Janos Guljas <janos@resenje.org>  Fri, 17 Feb 2012 04:27:28 +0100

transifex-client (0.6.1-1) unstable; urgency=low

  * New upstream release.

 -- Janos Guljas <janos@resenje.org>  Mon, 09 Jan 2012 14:27:43 +0100

transifex-client (0.6-1) unstable; urgency=low

  * New upstream release.
  * Add DM-Upload-Allowed control field.

 -- Janos Guljas <janos@resenje.org>  Tue, 20 Dec 2011 16:56:02 +0100

transifex-client (0.5.2-1) unstable; urgency=low

  * New upstream release.

 -- Janos Guljas <janos@resenje.org>  Fri, 24 Jun 2011 15:33:05 +0200

transifex-client (0.5-1) unstable; urgency=low

  * New upstream release. (Closes: #626982)
  * Bump standards version to 3.9.2.

 -- Janos Guljas <janos@resenje.org>  Tue, 17 May 2011 02:52:31 +0200

transifex-client (0.4.1-2) unstable; urgency=low

   * Team upload.
   * Rebuild to add Python 2.7 support

 -- Piotr Ożarowski <piotr@debian.org>  Sun, 08 May 2011 16:45:38 +0200

transifex-client (0.4.1-1) unstable; urgency=low

  * Initial release. (Closes: #608650)

 -- Janos Guljas <janos@resenje.org>  Sat, 22 Jan 2011 00:08:56 +0100
